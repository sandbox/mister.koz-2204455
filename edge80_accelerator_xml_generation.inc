<?php

/**
 *  Main XML generator class
 */
class edge80_accelerator_xml_config {
  var $header = '';
  var $footer = '';
  var $excludes = "";
  var $includes = "";

  /**
   *  Constructor
   */
  public function edge80_accelerator_xml_config() {
    //Set up methods
    $this->header = '<?xml version="1.0" ?>' . "\r\n" . '<resource xmlns="http://schema.modapt.com/pub/modapt-resource/1.0">';
    $this->footer = '</resource>';
  }

  /**
   *  Generator method
   */
  public function generate_routing_xml() {
    $match_host = "*";
    $routing = array();
    $routing[] = $this->header;
    $routing[] = "
  <metadata>
    <version>1.0</version>
  </metadata>
  <rule match-host='$match_host'>
    <delegate url='app/rules.xml' />
  </rule>";

    $routing[] = $this->footer;
    return implode("\r\n", $routing);
  }

  public function generate_config_xml() {
    $origin_site_hostname = variable_get('edge80_accelerator_origin_site_hostname', edge80_accelerator_config_default('edge80_accelerator_origin_site_hostname'));
    $origin_site_host = variable_get('edge80_accelerator_origin_site_host', edge80_accelerator_config_default('edge80_accelerator_origin_site_host'));
    $cache_images_lifetime = variable_get('edge80_accelerator_cache_images_lifetime', edge80_accelerator_config_default('edge80_accelerator_cache_images_lifetime'));
    $cache_assets_lifetime = variable_get('edge80_accelerator_cache_assets_lifetime', edge80_accelerator_config_default('edge80_accelerator_cache_assets_lifetime'));
    $cache_pages_lifetime = variable_get('edge80_accelerator_cache_pages_lifetime', edge80_accelerator_config_default('edge80_accelerator_cache_pages_lifetime'));

    $origin_site_hostname = preg_replace('#^https?://#', '', $origin_site_hostname);

    $config_xml = array();

    $config_xml[] = $this->header;
    $config_xml[] = '  <rule name="config.init" comment="Initialize the project">
    <prerequisite>
         <defmac name="origin_site_hostname" value="' . $origin_site_hostname . '"/>
         <defmac name="origin_site_host" value="' . $origin_site_host . '" />
        <!--<defmac name="asset_servers" value="apis.google.com, *.googleapis.com"/>-->
        <defmac name="asset_servers" value=""/>
      <defmac name="cache_images" value="' . $cache_images_lifetime . '"/>
      <defmac name="cache_assets" value="' . $cache_assets_lifetime . '"/>
      <defmac name="cache_pages" value="' . $cache_pages_lifetime . '"/>
    </prerequisite>
  </rule>';

    $config_xml[] = $this->footer;

    return implode("\r\n", $config_xml);
  }

  public function generate_rules_xml() {

    $rule_headers = $this->generate_rule_headers();
    // Never cache.
    $never_cache_items = edge80_accelerator_rule_get_list(EDGE80_ACCELERATOR_RULETYPE_NEVER);
    $never_cache_xml_text = $this->generate_rule_never_cache($never_cache_items);

    // Never cache and leave cookies
    $never_cache_leave_cookie_items = edge80_accelerator_rule_get_list(EDGE80_ACCELERATOR_RULETYPE_NEVER_COOKIES);
    $never_cache_leave_cookie_items = array_merge(edge80_accelerator_never_cache_system_urls(), $never_cache_leave_cookie_items);
    $never_cache_leave_cookie_xml_text = $this->generate_rule_never_cache_leave_cookies($never_cache_leave_cookie_items);

    // Block and redirect to 404
    $block_and_404_items = edge80_accelerator_rule_get_list(EDGE80_ACCELERATOR_RULETYPE_BLOCK);
    $block_and_404_items = array_merge(edge80_accelerator_block_system_urls(), $block_and_404_items);
    $block_and_404_xml_text = $this->generate_rule_block_and_redirect($block_and_404_items);


    $rules_xml = array();
    $rules_xml[] = $this->header;
    $rules_xml[] = '
  <include url="~/library/rulebook/1.0/framework.xml"/>
  <rule name="user.page_edits">
    <compose>
      <deliver if="content_has_errors || !content_is_html"/>
      <!-- Put any edits to html pages here -->
    </compose>
  </rule>
    ' . $rule_headers . '
    <!-- This is where i put all of the NEVER CACHE rules -->
    ' . $never_cache_xml_text . '
    <!-- This is where i put all of the NEVER CACHE AND LEAVE THE COOKIES ALONE alone rules -->
    ' . $never_cache_leave_cookie_xml_text . '
    <!-- This is where i put all of the BLOCK AND 404 rules -->
    ' . $block_and_404_xml_text . '

  <rule name="user.default" comment="Generic Page Processor"  match="*">
    <apply name="framework.proxy_edited" />
    <!-- While you can put page edits here, it is better to put them in user.page_edits -->
  </rule>';

    $rules_xml[] = $this->footer;

    return implode("\r\n", $rules_xml);
  }

  public function build_rule_url($rule_item) {
    $id = $rule_item[EDGE80_ACCELERATOR_DB_RULES_ID];
    $type = $rule_item[EDGE80_ACCELERATOR_DB_RULES_TYPE];
    $weight = $rule_item[EDGE80_ACCELERATOR_DB_RULES_WEIGHT];
    $value = $rule_item[EDGE80_ACCELERATOR_DB_RULES_VALUE];
    $wildcard = $rule_item[EDGE80_ACCELERATOR_DB_RULES_WILDCARD];

    $url = ltrim($value, "/");

    if ($wildcard) {
      $url .= "*";
    }

    return "/" . $url;
  }

  public function generate_rule_headers() {
    $rule_headers = '';
    //never cache
    $rule_headers .= <<<EOT

    <rule name="user.nocache" comment="No cache processor" match-path-re="">
      <apply name="framework.proxy_edited" />

      <set-cache maximum-age="0" aggressive="false" />

      <!-- While you can put page edits here, it is better to put them in user.page_edits -->

    </rule>

EOT;
    //never cache and don't squash cookies
    $rule_headers .= <<<EOT

    <rule name="user.managed" comment="Managed Cookie Processor" match-path="">
      <apply name="framework.managed.proxy_edited" />

      <!-- Pages matched by this rule will respect any cookies delivered or set during the request and response. -->

      <!-- While you can put page edits here, it is better to put them in user.page_edits -->

    </rule>

EOT;
    //404
    $location_404 = edge80_accelerator_get_404_url();
    $rule_headers .= <<<EOT

    <rule name="user.404" comment="Redirect to 404 page">
      <prerequisite>
        <script>
          dombuf.headers.setHeader('Location', '$location_404');
          document.responseCode = 302;
        </script>
      </prerequisite>
    </rule>

EOT;
    return $rule_headers;
  }

  public function generate_rule_never_cache($items = array()) {
    $urls = array();
    foreach ($items as $item) {
      $urls[] = $this->build_rule_url($item);
    }
    $output = "";

    foreach ($urls as $url) {
      $output .= <<<EOT

    <rule comment="No cache processor" match-path="$url">
      <apply name="user.nocache" />
    </rule>
EOT;
    }
    return $output;
  }

  public function generate_rule_never_cache_leave_cookies($items = array()) {
    $urls = array();
    foreach ($items as $item) {
      $urls[] = $this->build_rule_url($item);
    }
    $output = "";

    foreach ($urls as $url) {
      $output .= <<<EOT

    <rule comment="Managed Cookie Processor" match-path="$url">
      <apply name="user.managed" />
    </rule>
EOT;
    }
    return $output;
  }

  public function generate_rule_block_and_redirect($items = array()) {
    $urls = array();
    foreach ($items as $item) {
      $urls[] = $this->build_rule_url($item);
    }
    $output = "";

    foreach ($urls as $url) {
      $output .= <<<EOT

    <rule comment="Redirect to 404 page" match-path="$url">
      <apply name="user.404" />
    </rule>
EOT;
    }
    return $output;
  }
}
