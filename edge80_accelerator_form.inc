<?php
include_once("edge80_accelerator.methods.inc");

/**
 * System settings for Edge80 cache.
 */

function edge80_accelerator_settings_form($form, &$form_state, $no_js_use = FALSE)  {

  // General settings
  $form['global_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Account and global settings.'),
  );

  $form['global_settings']['edge80_accelerator_origin_site_hostname'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Domain'),
    '#default_value' => variable_get('edge80_accelerator_origin_site_hostname',edge80_accelerator_config_default('edge80_accelerator_origin_site_hostname')),
    '#description' => t('Please enter the hostname of the backend site (this site).'),
  );

  $form['global_settings']['edge80_accelerator_origin_site_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Site IP Address'),
    '#default_value' => variable_get('edge80_accelerator_origin_site_host',edge80_accelerator_config_default('edge80_accelerator_origin_site_host')),
    '#description' => t('Please enter the IP address of the backend site (this site).'),
  );

  $form['global_settings']['edge80_accelerator_cache_pages_lifetime'] = array(
    '#type' => 'select',
    '#options' => edge80_accelerator_ttl_list(),
    '#title' => t('Pages cache lifetime'),
    '#default_value' => variable_get('edge80_accelerator_cache_pages_lifetime',edge80_accelerator_config_default('edge80_accelerator_cache_pages_lifetime')),
    '#description' => t('The amount of time (seconds) that Edge80 should cache normal pages that are not covered by other rules.'),
  );

  $form['global_settings']['edge80_accelerator_cache_images_lifetime'] = array(
    '#type' => 'select',
    '#options' => edge80_accelerator_ttl_list(),
    '#title' => t('Image cache lifetime'),
    '#default_value' => variable_get('edge80_accelerator_cache_images_lifetime',edge80_accelerator_config_default('edge80_accelerator_cache_images_lifetime')),
    '#description' => t('The amount of time in seconds that Edge80 should cache images.'),
  );

  $form['global_settings']['edge80_accelerator_cache_assets_lifetime'] = array(
    '#type' => 'select',
    '#options' => edge80_accelerator_ttl_list(),
    '#title' => t('Asset cache lifetime'),
    '#default_value' => variable_get('edge80_accelerator_cache_assets_lifetime',edge80_accelerator_config_default('edge80_accelerator_cache_assets_lifetime')),
    '#description' => t('The amount of time in seconds that Edge80 should cache other files (JS, CSS etc).'),
  );

  $form['global_settings']['edge80_accelerator_404_node'] = array(
    '#type' => 'textfield',
    '#title' => t('404 node'),
    '#description' => t('Please enter the node/page path you would like displayed if url is set to "block and redirect to 404", leave blank to use Drupal\'s default.'),
    '#default_value' => variable_get('edge80_accelerator_404_node', edge80_accelerator_config_default('edge80_accelerator_404_node')),
  );

  // Edge80 cache rules
  $form['cache_rules'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache rules.'),
    '#tree' => TRUE,
    '#prefix' => '<div id="edge80_accelerator_rules_wrapper">',
    '#suffix' => '</div>',
  );

  $rules = edge80_accelerator_rule_get_list();
  $existing_rules = count($rules);

  $form_state['storage']['forms'] = isset($form_state['storage']['forms']) ? $form_state['storage']['forms'] : $existing_rules;

  if (isset($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'add_rule') {
    $form_state['storage']['forms']++;
  }

  for($i = 0; $i < $existing_rules; $i++) {
    $form['cache_rules']["rule_" . $i] = edge80_accelerator_rule_make_form($i, $form_state['storage']['forms'], $rules[$i]);
  }

  if ($form_state['storage']['forms']) {
    for ($i = $existing_rules; $i < $form_state['storage']['forms']; $i++) {
      $form['cache_rules']["rule_" . $i] = edge80_accelerator_rule_make_form($i, $form_state['storage']['forms']);
    }
  }

  $form['add_rule'] = array(
    '#type' => 'button',
    '#value' => t('Add rule'),
    '#name' => 'add_rule',
    '#href' => '',
    '#ajax' => array(
      'callback' => 'edge80_accelerator_form_router_callback',
      'wrapper' => 'edge80_accelerator_rules_wrapper',
    ),
  );

  $form['submit'] = array(
    '#value' => t('Save'),
    '#type' => 'submit',
    '#name' => "save",
  );

  return $form;
}

function edge80_accelerator_form_swap_callback(&$form, &$form_state, $record_number_first, $record_number_second) {
  $GLOBALS['conf']['cache'] = FALSE;

  $first_record_key = 'rule_' . $record_number_first;
  $second_record_key = 'rule_' . $record_number_second;

  $first_record = edge80_accelerator_rule_update_record_number($form_state['values']['cache_rules'][$first_record_key], $record_number_second, $form_state_state['storage']['forms']);
  $second_record = edge80_accelerator_rule_update_record_number($form_state['values']['cache_rules'][$second_record_key], $record_number_first, $form_state['storage']['forms']);

  $form['cache_rules'][$first_record_key] = $second_record;
  $form['cache_rules'][$second_record_key] = $first_record;
  return $form['cache_rules'];
}

function edge80_accelerator_form_delete_rule_callback(&$form, &$form_state, $record_number) {
  $GLOBALS['conf']['cache'] = FALSE;
  if (!empty($form_state['triggering_element'])) {
    $selected_rule_id =  $form['cache_rules']["rule_" . $record_number]['rule_id']['#value'];
    if (!empty($selected_rule_id)) {
      edge80_accelerator_rule_delete($selected_rule_id);
    }
    unset($form['cache_rules']["rule_" . $record_number]);
  }
  return $form['cache_rules'];
}

function edge80_accelerator_form_router_callback(&$form, &$form_state) {
  $GLOBALS['conf']['cache'] = FALSE;
  if (!empty($form_state['triggering_element'])) {
    $record_name = $form_state['triggering_element']['#name'];
    $record_deets = explode("_", $record_name);
    $record_number = $record_deets[1];
    $method = $record_deets[0];

    switch ($method) {
      case 'move-up-rule':
        //move up rule
        return edge80_accelerator_form_swap_callback($form, $form_state, $record_number, $record_number - 1);
        break;
      case 'move-down-rule':
        //move down rule
        return edge80_accelerator_form_swap_callback($form, $form_state, $record_number, $record_number + 1);
        break;
      case 'delete-rule':
        //delete rule
        return edge80_accelerator_form_delete_rule_callback($form, $form_state, $record_number);
        break;
      case 'add':
        //add row
        $form_state['storage']['forms']++;
        return $form['cache_rules'];
        break;
    }
  }
}

// and this is teh submit
function edge80_accelerator_settings_form_submit($form, &$form_state) {
  if ($form_state['triggering_element']['#name'] == "save") {
    //manual validation
    $form_origin_site_hostname = trim($form_state['values']['edge80_accelerator_origin_site_hostname']);
    $form_origin_site_host = trim($form_state['values']['edge80_accelerator_origin_site_host']);
    $form_cache_pages_lifetime = trim($form_state['values']['edge80_accelerator_cache_pages_lifetime']);
    $form_cache_images_lifetime = trim($form_state['values']['edge80_accelerator_cache_images_lifetime']);
    $form_cache_assets_lifetime = trim($form_state['values']['edge80_accelerator_cache_assets_lifetime']);
    $form_404_node = $form_state['values']['edge80_accelerator_404_node'];

    //validate :edge80_accelerator_origin_site_hostname
    if (!valid_url($form_origin_site_hostname, TRUE)) {
      form_set_error('global_settings][edge80_accelerator_origin_site_hostname', t("Error validating Site Domain, please ensure a full URL is entered here."));
    }

    //validate :edge80_accelerator_origin_site_host
    if (!edge80_accelerator_parse_ip($form_origin_site_host)) {
      form_set_error('global_settings][edge80_accelerator_origin_site_host', t("Error validating site IP, please ensure a valid IP address is entered here."));
    }

    // Rules validation
    $i = 0;
    foreach ($form_state['values']['cache_rules'] as $key => $item) {
      if (is_array($item) && strpos($key, 'rule_') !== FALSE) {
        $item_path = $item['url_path'];
        if (!edge80_accelerator_rule_validate($item, $form_origin_site_hostname)) {
          form_set_error(
            "cache_rules][" . $key,
            "Error validating the rule \"" . $item_path . "\" please ensure the domain is entered correctly and the path is valid");
        }
      }
    }

    if (form_get_errors()) return;

    variable_set('edge80_accelerator_origin_site_hostname', $form_origin_site_hostname);
    variable_set('edge80_accelerator_origin_site_host', $form_origin_site_host);
    variable_set('edge80_accelerator_cache_images_lifetime', $form_cache_images_lifetime);
    variable_set('edge80_accelerator_cache_assets_lifetime', $form_cache_assets_lifetime);
    variable_set('edge80_accelerator_cache_pages_lifetime', $form_cache_pages_lifetime);
    variable_set('edge80_accelerator_404_node', $form_404_node);

    // do the actual variable saves
    // Rules Saving
    foreach ($form_state['values']['cache_rules'] as $key => $item) {
      if (is_array($item) && strpos($key, 'rule_') !== FALSE) {
        edge80_accelerator_rule_save($item);
      }
      else {
      }
    }
  }
}
