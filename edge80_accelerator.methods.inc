<?php

require_once("edge80_accelerator_defines.inc");
require_once("edge80_accelerator_xml_generation.inc");

/**
 *  Call the main XML generator methods.
 */

function edge80_generate_routing_xml() {
  $GLOBALS['conf']['cache'] = FALSE;
  $xml = new edge80_accelerator_xml_config();
  drupal_add_http_header('Content-Type', 'application/xml');
  $response = $xml->generate_routing_xml();
  echo $response;
  drupal_exit();
}
function edge80_generate_config_xml() {
  $GLOBALS['conf']['cache'] = FALSE;
  $xml = new edge80_accelerator_xml_config();
  drupal_add_http_header('Content-Type', 'application/xml');
  $response = $xml->generate_config_xml();
  echo $response;
  drupal_exit();
}
function edge80_generate_rules_xml() {
  $GLOBALS['conf']['cache'] = FALSE;
  $xml = new edge80_accelerator_xml_config();
  $response = $xml->generate_rules_xml();
  drupal_add_http_header('Content-Type', 'application/xml');
  echo $response;
  drupal_exit();
}
/**
 * Implemented in un-install - removes of the variables.
 */
function edge80_accelerator_delete_all_variables() {
  $config_variables = edge80_accelerator_config_options();

  foreach ($config_variables as $value) {
    if (is_array($value)) {
      variable_del($value[0]);
    }
  }
}

/**
 * Implemented in install - sets up all of the variables.
 */
function edge80_accelerator_write_all_variables() {
  $config_variables = edge80_accelerator_config_options();

  foreach ($config_variables as $value) {
    if (is_array($value)) {
      variable_set($value[0], $value[1]);
    }
  }
}

function edge80_accelerator_config_options() {
  Global $base_url;
  $config = array();
  $config['edge80_accelerator_origin_site_hostname'] = $base_url;
  $config['edge80_accelerator_origin_site_host'] = "";
  $config['edge80_accelerator_cache_images_lifetime'] = "86400";
  $config['edge80_accelerator_cache_assets_lifetime'] = "86400";
  $config['edge80_accelerator_cache_pages_lifetime'] = "7200";
  $config['edge80_accelerator_404_node'] = "node/0";
  return $config;
}

function edge80_accelerator_config_default($itemname) {
    $config = edge80_accelerator_config_options();

    if (array_key_exists($itemname, $config)) {
        return $config[$itemname];
    }
    return;
}

function edge80_accelerator_never_cache_system_urls() {
  $uid = EDGE80_ACCELERATOR_DB_RULES_ID;
  $ruletype = EDGE80_ACCELERATOR_DB_RULES_TYPE;
  $weight = EDGE80_ACCELERATOR_DB_RULES_WEIGHT;
  $rulevalue = EDGE80_ACCELERATOR_DB_RULES_VALUE;
  $rulewildcard = EDGE80_ACCELERATOR_DB_RULES_WILDCARD;

  $never_cache_urls = array();
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/includes/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/misc/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/modules/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/profiles/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/scripts/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/themes/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/cron.php", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/update.php", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/xmlrpc.php", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/admin/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/comment/reply/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/filter/tips/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/node/add/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/search/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/user/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/user/register/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/user/password/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/user/login/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/user/logout/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=admin/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=comment/reply/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=filter/tips/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=node/add/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=search/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=user/password/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=user/register/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=user/login/", $rulewildcard => TRUE);
  $never_cache_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_NEVER, $weight => 0, $rulevalue => "/?q=user/logout/", $rulewildcard => TRUE);

  return $never_cache_urls;
}

function edge80_accelerator_block_system_urls() {
  $uid = EDGE80_ACCELERATOR_DB_RULES_ID;
  $ruletype = EDGE80_ACCELERATOR_DB_RULES_TYPE;
  $weight = EDGE80_ACCELERATOR_DB_RULES_WEIGHT;
  $rulevalue = EDGE80_ACCELERATOR_DB_RULES_VALUE;
  $rulewildcard = EDGE80_ACCELERATOR_DB_RULES_WILDCARD;

  $block_urls = array();
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/CHANGELOG.txt", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/INSTALL.mysql.txt", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/INSTALL.pgsql.txt", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/INSTALL.sqlite.txt", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/install.php", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/INSTALL.txt", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/LICENSE.txt", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/MAINTAINERS.txt", $rulewildcard => TRUE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/UPGRADE.txt", $rulewildcard => TRUE);

  //now the edge80 xml files!
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/edge80/app/config.xml", $rulewildcard => FALSE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/edge80/app/rules.xml", $rulewildcard => FALSE);
  $block_urls[] = array($uid => -1, $ruletype => EDGE80_ACCELERATOR_RULETYPE_BLOCK, $weight => 0, $rulevalue => "/edge80/routing.xml", $rulewildcard => FALSE);

  return $block_urls;
}

function edge80_accelerator_get_node_list() {
  $denied_node_ids = db_select('node', 'n')
      ->fields('n', array('nid'))
      ->fields('n', array('title'))
      ->fields('n', array('type'))
      ->execute()
      ->fetchCol();
  $denied_nodes_possible_options = node_load_multiple($denied_node_ids);
  $denied_node_options = array('0' => "use drupal's 404 url");
  foreach ($denied_nodes_possible_options as $nodeid => $node) {
    $denied_node_options[$nodeid] = $node->title;
  }

  return $denied_node_options;
}


/**
 * Helper method: get existing rules.
 */
function edge80_accelerator_rule_get_list($type = '') {
  $result = array();
  $query_string = "SELECT
    " . EDGE80_ACCELERATOR_DB_RULES_ID . ",
    " . EDGE80_ACCELERATOR_DB_RULES_TYPE . ",
    " . EDGE80_ACCELERATOR_DB_RULES_VALUE . ",
    " . EDGE80_ACCELERATOR_DB_RULES_WEIGHT . ",
    " . EDGE80_ACCELERATOR_DB_RULES_WILDCARD . "  FROM {" . EDGE80_ACCELERATOR_DB_RULES_TABLE . "}";

  if (!empty($type)) {
    $query_string .= " WHERE " . EDGE80_ACCELERATOR_DB_RULES_TYPE . " = '$type'";
  }
  $query_string .= " ORDER BY " . EDGE80_ACCELERATOR_DB_RULES_WEIGHT . " ASC";


  $query = db_query($query_string);
  while ($row = $query->fetchAssoc()) {
    $result[] = $row;
  }
  return $result;
}

function edge80_accelerator_rule_delete($id) {
  $num_deleted = db_delete(EDGE80_ACCELERATOR_DB_RULES_TABLE)
  ->condition(EDGE80_ACCELERATOR_DB_RULES_ID, $id)
  ->execute();
  return $num_deleted;
}

/**
 * Helper method: validate rule inputs.
 */
function edge80_accelerator_rule_validate($item, $domain) {
  $item_path = $item['url_path'];
  return (!empty($item_path) && valid_url($domain . $item_path, TRUE));
}

/**
 * Helper method: save rule inputs.
 */
function edge80_accelerator_rule_save($item) {
  if (!empty($item['rule_id'])) {
    $num_updated = db_update(EDGE80_ACCELERATOR_DB_RULES_TABLE)
    ->fields(array(
      EDGE80_ACCELERATOR_DB_RULES_TYPE => $item['rule_type'],
      EDGE80_ACCELERATOR_DB_RULES_VALUE => $item['url_path'],
      EDGE80_ACCELERATOR_DB_RULES_WILDCARD => $item['rule_wildcard'],
      EDGE80_ACCELERATOR_DB_RULES_WEIGHT => $item['weight'],
    ))
    ->condition(EDGE80_ACCELERATOR_DB_RULES_ID, $item['rule_id'], '=')
    ->execute();
    return $num_updated;
  }
  else {
    $saved_id = db_insert(EDGE80_ACCELERATOR_DB_RULES_TABLE)
    ->fields(array(
        EDGE80_ACCELERATOR_DB_RULES_TYPE => $item['rule_type'],
        EDGE80_ACCELERATOR_DB_RULES_VALUE => $item['url_path'],
        EDGE80_ACCELERATOR_DB_RULES_WILDCARD => $item['rule_wildcard'],
        EDGE80_ACCELERATOR_DB_RULES_WEIGHT => $item['weight'],
    ))
    ->execute();
    return $saved_id;
  }
}

/**
 * Helper method: get cache rule types.
 */
function edge80_accelerator_rule_type_list() {
  $rule_types = array(
    EDGE80_ACCELERATOR_RULETYPE_NEVER => "Never Cache",
    EDGE80_ACCELERATOR_RULETYPE_NEVER_COOKIES => "Never Cache and leave cookies",
    EDGE80_ACCELERATOR_RULETYPE_BLOCK => "Block redirect to 404 page",
  );

  return $rule_types;
}

/**
 * Helper method: generate rule config form item
 */
function edge80_accelerator_rule_make_form($record_number, $max, $item = '') {
  $isfirst = ($record_number == 0? TRUE : FALSE);
  $islast = ($record_number >= $max -1? TRUE : FALSE);

  $item_field_url_path = '';
  $item_field_rule_type = 0;
  $item_field_rule_wildcard = TRUE;
  $item_field_rule_id = '';
  $item_field_rule_weight = $record_number;

  if (is_array($item)) {
    $item_field_url_path = $item[EDGE80_ACCELERATOR_DB_RULES_VALUE];
    $item_field_rule_type = $item[EDGE80_ACCELERATOR_DB_RULES_TYPE];
    $item_field_rule_wildcard = $item[EDGE80_ACCELERATOR_DB_RULES_WILDCARD];
    $item_field_rule_id = $item[EDGE80_ACCELERATOR_DB_RULES_ID];
    $item_field_rule_weight = $item[EDGE80_ACCELERATOR_DB_RULES_WEIGHT];
  }

  $item = array();

  // Rule URL path.
  $item['url_path'] = array(
    '#title' => t('Path'),
    '#type' => 'textfield',
    '#default_value' => $item_field_url_path,
    '#prefix' => '<div class="container-inline">',
  );

  // Rule type.
  $item['rule_type'] = array(
    '#type' => 'select',
    '#default_value' => $item_field_rule_type,
    '#options' => edge80_accelerator_rule_type_list(),
  );

  // Rule wild card
  $item['rule_wildcard'] = array(
    '#type' => 'checkbox',
    '#title' => t('Wildcard?'),
    '#default_value' => $item_field_rule_wildcard,
  );

  $item['rule_delete'] = array(
    '#type' => 'button',
    '#value' => t('x'),
    '#name' => 'delete-rule_'.$record_number,
    '#href' => '',
    '#ajax' => array(
      'callback' => 'edge80_accelerator_form_router_callback',
      'wrapper' => 'edge80_accelerator_rules_wrapper',
    ),
  );

  $item['rule_move_up'] = array(
    '#type' => 'button',
    '#value' => t('up'),
    '#name' => 'move-up-rule_'.$record_number,
    '#href' => '',
    '#ajax' => array(
      'callback' => 'edge80_accelerator_form_router_callback',
      'wrapper' => 'edge80_accelerator_rules_wrapper',
    ),
  );

  if ($isfirst) {
    $item['rule_move_up']['#disabled'] = TRUE;
  }
  else {
    $item['rule_move_up']['#disabled'] = FALSE;
  }

  $item['rule_move_down'] = array(
    '#type' => 'button',
    '#value' => t('down'),
    '#name' => 'move-down-rule_'.$record_number,
    '#href' => '',
    '#ajax' => array(
      'callback' => 'edge80_accelerator_form_router_callback',
      'wrapper' => 'edge80_accelerator_rules_wrapper',
    ),
  );

  if ($islast) {
    $item['rule_move_down']['#disabled'] = TRUE;
  }
  else {
    $item['rule_move_down']['#disabled'] = FALSE;
  }

  $item['weight'] = array(
    '#type' => 'hidden',
    '#title' => t('hidden'),
    '#default_value' => $item_field_rule_weight,
    '#title_display' => 'invisible',
  );

  $item['record_number'] = array(
    '#type' => 'hidden',
    '#title' => t('hidden'),
    '#default_value' => $record_number,
    '#title_display' => 'invisible',
  );

    // Rule ID field for update calls.
  $item['rule_id'] = array(
    '#title' => t('Rule ID'),
    '#type' => 'hidden',
    '#default_value' => $item_field_rule_id,
    '#suffix' => '</div>',
  );

  return $item;

}

function edge80_accelerator_rule_update_record_number($record, $record_number, $max) {
  $isfirst = ($record_number == 0? TRUE : FALSE);
  $islast = ($record_number == $max -1? TRUE : FALSE);

  $record['rule_delete']['#name'] = 'delete-rule_'.$record_number;
  $record['weight']['#value'] = $record_number;

  if (!empty($record['rule_id']['#value'])) {
      edge80_accelerator_rule_save($record);
  }

  $record['rule_move_up']['#name'] = 'move-up-rule_'.$record_number;
  $record['rule_move_up']['#disabled'] = $isfirst;

  $record['rule_move_down']['#name'] = 'move-down-rule_'.$record_number;
  $record['rule_move_down']['#disabled'] = $islast;


  return $record;
}

/**
 * Tooling method: Rudimentry ip parsing test.
 */
function edge80_accelerator_parse_ip($ip_address) {
  if (preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/", $ip_address)) {
    $parts = explode(".", $ip_address);
    foreach ($parts as $part) {
      if (intval($part) > 255 || intval($part) < 0) {
        return FALSE;
      }
    }
    return TRUE;
  }
  return FALSE;
}

/**
 * Tooling method: get a list of TTL's
 */
function edge80_accelerator_ttl_list() {
  $options = array(
    "0" => "0s (disable)",
    "300" => "300 (5 minutes)",
    "3600" => "3600 (1 hour)",
    "7200" => "7200 (2 hours)",
    "14400" => "14400 (4 hours)",
    "28800" => "28800 (8 hours)",
    "57600" => "57600 (16 hours)",
    "86400" => "86400 (1 day)",
    "172800" => "172800 (2 days)",
    "345600" => "345600 (4 days)",
  );

  return $options;
}

function edge80_accelerator_get_404_url() {
  $node_id = variable_get('edge80_accelerator_404_node', edge80_accelerator_config_default('edge80_accelerator_404_node'));

  if (empty($node_id)) {
    return "/" . variable_get('site_404');
  }

  return $node_id;
}
